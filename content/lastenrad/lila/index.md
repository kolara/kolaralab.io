+++
title = "Lila Lastenrad"
description = "Long-John"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long-John"},
  {name = "Standort", entry = "Pörstener Straße, Kleinzschocher"},
  {name = "Beladung", entry = "100 kg"},
  {name = "Email", entry = "lila@kolara.org"},
  {name = "Details", entry = "7-Gang-Nabenschaltung, Scheibenbremse vorne, Trommelbremse hinten, Licht"},
]
lat = "51.313164429019004"
lon = "12.325684850888859"
image = "lila2018.jpg"
+++
