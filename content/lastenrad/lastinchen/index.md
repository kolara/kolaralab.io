+++
title = "Lastinchen"
description = "Fahrrad mit Frontgepäckträger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Frontgepäck"},
  {name = "Standort", entry = "Schleußig"},
  {name = "Beladung", entry = "20 kg"},
  {name = "Email", entry = "lastinchen@kolara.org"},
  {name = "Details", entry = "3-Gang-Nabenschaltung, Rücktrittbremse, Licht"},
]
lat = "51.330573606069265"
lon = "12.344926946881682"
+++
Eine Transportkiste kann mit ausgeliehen werden


