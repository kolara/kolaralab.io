+++
title = "Kupferblitz"
description = "Long-John"
template = "zeug.html"

[extra]
thumbnail = "image.jpg"
properties = [
  {name = "Bautyp", entry = "Long-John"},
  {name = "Standort", entry = "Lützner Str./Ecke Odermannstr."},
  {name = "Beladung", entry = "bis 80 kg"},
  {name = "Email", entry = "kupferblitz@kolara.org"},
  {name = "Details", entry = "21-Gang-Kettenschaltung, Felgenbremse vorne/hinten"},
]
lat = "51.33570269628167"
lon = "12.330224348406055"
+++
Das Rad steht bei der NoBorderBiking Selbsthilfewerkstatt. Unter folgender Nummer könnt ihr auch nachfragen, ob das Rad verfügbar ist: 0162-701 21 58
Die Werkstatt ist normalerweise Di-Fr 15-19 Uhr offen, und ihr könnt auch einfach vorbeikommen.
{{ gallery() }}
