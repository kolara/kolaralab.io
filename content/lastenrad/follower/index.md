+++
title = "Follower"
description = "Anhänger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Hans-Driesch-Straße,Leutzsch"},
  {name = "Beladung", entry = "90 kg; 150 kg als Handwagen"},
  {name = "Email", entry = "meinfollower@kolara.org"},
  {name = "Details", entry = "Ladefläche: 128x62x8 cm"},
]
lat = "51.345966878285914"
lon = "12.311753877551297"
+++
Kupplungen: Weber-Kupplungen, Griff für die Nutzung als Handwagen
(https://shop.hinterher.com/Einzelteile/Kupplungen/) </br>
Zubehör:
  * Aluminium-Box (Innenmaß: 642x477x380 mm, abschließbar, Deckel
nicht abnehmbar, spritzwassergeschützt)
  * Multi-Frame (Reling) 
  * Stützrad für den Handwagenbetrieb

Anmeldung:
Bitte melde dich bei Eric, um einen Termin für ein Treffen zu
vereibaren. Bitte bringe ein offizielles Dokument mit (Ausweis oder
Pass). Dieses werden wir als Sicherheit Kopieren/Photographieren.
Als nächstes stellt sich noch die Frage nach der Kupplung (falls du den
Anhänger mit dem Fahrrad ziehen willst).
Hier gibt es drei Optionen:
1) Du kaufst dir eine eigene Kupplung
2) Du leihst die Kupplung (2€ Spendenempfehlung pro Nutzung) und
montierst sie an dein Fahrrad