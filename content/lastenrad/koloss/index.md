+++
title = "Koloss von Reudnitz"
description = "Anhänger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Gregor-Fuchs-Straße, Anger Crottendorf"},
  {name = "Beladung", entry = "250+ kg"},
  {name = "Email", entry = "koloss@kolara.org"},
  {name = "Details", entry = "schwerer Anhänger mit Auflaufbremse, Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.33725598841647"
lon = "12.415394952001009"
+++
Der Koloss ist ein einachsiger Anhänger mit Standardkupplung und integrierter Auflaufbremse (nicht für alle Räder geeignet).
Mit einer Ladefläche von 2,50m x 1m und einer Nutzlast von +250kg ein must-have um während des Umzugs auch noch das gerade gefundene Sofa von der Straße retten zu können.


