+++
title = "Stötterad"
description = "Long-John"
template = "zeug.html"

[extra]
thumbnail = "image.jpg"
properties = [
  {name = "Bautyp", entry = "Long-John, Bakfiets short"},
  {name = "Standort", entry = "Ferdinand-Jost Straße, Stötteritz"},
  {name = "Beladung", entry = "bis 100 kg, ideal für ein größeres oder 2 kleinere Kinder"},
  {name = "Email", entry = "stoetterad@kolara.org"},
  {name = "Details", entry = "8-fach Schaltung, kurz und wendig, Sitzbank und Anschnallgurte dabei"},
]
lat = "51.322216173619736"
lon = "12.422565927307716"
+++
Zum Kindertransport gut geeignet, auch für Anfänger_Innen. Die Sitzbank kann hochgeklappt werden, um mehr Platz für größere Dinge zu schaffen. Zuladung Box max. 80 kg; Zuladung Gepäckträger max. 50 kg; technische Daten siehe [hier](https://mycargobike.de/produkte/bakfiets-cargo-bike-short/):
{{ gallery() }}
