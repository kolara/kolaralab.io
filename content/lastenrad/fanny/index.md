+++
title = "Fanny"
description = "Dreirad"
template = "zeug.html"

[extra]
thumbnail = "image.jpg"
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Connewitz"},
  {name = "Beladung", entry = "bis 180 kg"},
  {name = "Email", entry = "fanny@kolara.org"},
  {name = "Details", entry = "Scheibenbremsen, Nabenschaltung, Licht und ein platter Reifen"},
]
lat = "51.30959318456205"
lon = "12.37351888319611"
+++
{{ gallery() }}

