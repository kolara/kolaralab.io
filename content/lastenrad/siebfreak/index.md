+++
title = "Siebfreak"
description = "Dreirad"
template = "zeug.html"

[extra]
thumbnail = "siebfreak.jpg"
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Zschochersche Straße, Plagwitz"},
  {name = "Beladung", entry = "200kg"},
  {name = "Email", entry = "siebfreak@kolara.org"},
  {name = "Details", entry = "7-Gang Nabenschaltung mit Rücktrittbremse, Nabendynamo"},
]
lat = "51.332037987795104"
lon = "12.338632187933719"
+++


**Siebfreak** ist eines der ältesten Lastenräder im Kolara.
