+++
title = "Die Zwei"
description = "Tandem"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Tandem"},
  {name = "Standort", entry = "Reichpietschstraße, Reudnitz"},
  {name = "Beladung", entry = "1 Mitfahrende*r"},
  {name = "Email", entry = "diezwei@kolara.org"},
  {name = "Details", entry = "5-Gang-Nabenschaltung, Rücktrittbremse, Licht"},
]
lat = "51.334648563531616"
lon = "12.39839947262932"
+++
Für den Personentransport – auch für Personen, die nicht als Last empfunden werden

