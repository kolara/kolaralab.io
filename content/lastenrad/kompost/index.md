
+++
title = "KompOst-Lastenrad"
description = "Long-John"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long-John"},
  {name = "Standort", entry = "Sellerhäuserstraße, Anger-Crottendorf"},
  {name = "Beladung", entry = "100kg"},
  {name = "Email", entry = "kompost@kolara.org"},
  {name = "Details", entry = "Antrieb, Bremse, Schutzbleche"},
]
lat = "51.337278227500484"
lon = "12.415752159952833"
+++
Entstanden während des Workshops zusammen mit einer Hochschulgruppe ist das einspurige Rad, Modell "Long-John", treuer Begleiter für mobile Küfas, Alltag oder den Ausflug an den See. Auf die Ladefläche (L 0,9m/ B 0,7m) passen hintereinander 2 Standard-Bäckerkisten - oder 4 Kästen Hopfenbrause.
