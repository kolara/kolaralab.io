+++
title = "Streitwagen"
description = "Stahlkoloss"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "unbekannt"},
  {name = "Beladung", entry = "120 kg"},
  {name = "Email", entry = "streitwagen@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.34745480631303"
lon = "12.409665626438745"
+++
sehr stabil. Leider wurde der Anhänger gestohlen. Wer den Anhänger sieht, meldet sich bitte bei uns.
