+++
title = "Kleiner Onkel"
description = "Dreirad"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Mariannenstraße"},
  {name = "Beladung", entry = "100 kg"},
  {name = "Email", entry = "kleineronkel@kolara.org"},
  {name = "Details", entry = "5-Gang-Nabenschaltung, Lenkungsdämpfer, Rücktrittbremse, Licht"},
]
lat = "51.3468707816748"
lon = "12.406103832101559"
+++
gelb
