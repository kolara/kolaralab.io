+++
title = "Ralf"
description = "Anhänger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Heinrichstraße, Reudnitz"},
  {name = "Beladung", entry = "90 kg"},
  {name = "Email", entry = "ralf@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.337720984185"
lon = "12.401328168915988"
+++
stilvoll im Retro-Chic. Bei Bedarf gibt es einen Holzaufsatz (eher für Leichtes-Volumniöses) dazu.

