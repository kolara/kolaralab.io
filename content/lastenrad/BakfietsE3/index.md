+++
title = "Bakfiets E-3-Rad"
description = "Dreirad"
template = "zeug.html"

[extra]
thumbnail = "image.jpg"
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Lützner Str./Ecke Odermannstr."},
  {name = "Beladung", entry = "bis 100 kg"},
  {name = "Email", entry = "bakfiets@kolara.org"},
  {name = "Details", entry = "18-Gang-Ketten-Nabenschaltung, Scheibenbremse vorne, Rücktritt hinten"},
]
lat = "51.335824212644084"
lon = "12.329909439949482"
+++
Das Rad steht bei der NoBorderBiking Selbsthilfewerkstatt. Unter folgender Nummer könnt ihr auch nachfragen, ob das Rad verfügbar ist: 0162-701 21 58
Die Werkstatt ist normalerweise Di-Fr 15-19 Uhr offen, und ihr könnt auch einfach vorbeikommen.
{{ gallery() }}
