+++
title = "Frau Werner"
description = "großer Frontgepäckträger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Frontgepäck"},
  {name = "Standort", entry = "Planetarium, Jena"},
  {name = "Beladung", entry = "40 kg"},
  {name = "Email", entry = "frauwerner@kolara.org"},
  {name = "Details", entry = "Kettenschaltung, unterschiedlich große Räder, Last lenkt mit"},
]
lat = "50.93227585335921"
lon = "11.588618233893293"
+++
Komplettrecycling, kurzer Rahmen, niedriger Schwerpunkt, daher extrem wendig. Erobert als erstes Kollektiv-Rad Dresden
