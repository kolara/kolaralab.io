+++
title = "Krummelus"
description = "Türgestell"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Ostwache, Radsfatz"},
  {name = "Beladung", entry = "200 kg"},
  {name = "Email", entry = "krummelus@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.33661862645704"
lon = "12.419331682966199"
+++
Komplettrecycling, Sehr große Ladefläche, exzellent für große sperrige, aber eher leichte Dinge.

