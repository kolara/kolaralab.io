+++
title = "Snake"
description = "Long-John"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long-John"},
  {name = "Standort", entry = "Friesenstraße, Lindenau"},
  {name = "Beladung", entry = "100 kg"},
  {name = "Email", entry = "snake@kolara.org"},
  {name = "Details", entry = "7-Gang-Nabenschaltung, Hydraulische Bremse"},
]
lat = "51.3420888298041"
lon = "12.330461852014308"
+++
Bei voller Beladung schwerfällige Lenkung
