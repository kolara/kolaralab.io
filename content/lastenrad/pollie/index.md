+++
title = "Pollie Pollerfräse"
description = "großer Schwerlastanhänger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Schwerlastanhänger"},
  {name = "Standort", entry = "Radsfatz, Ostwache"},
  {name = "Beladung", entry = "150 kg+"},
  {name = "Email", entry = "pollie@kolara.org"},
  {name = "Details", entry = "Ladefläche: 103 x 207 cm"},
]
lat = "51.33625"
lon = "12.4190737"
+++
Wie der Name schon impliziert, ist der Anhänger mit 1,37 m breiter als der übliche Pollerabstand. 
