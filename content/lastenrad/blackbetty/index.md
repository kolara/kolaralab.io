+++
title = "Black Betty"
description = "Anhänger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "unbekannt"},
  {name = "Beladung", entry = "90 kg"},
  {name = "Email", entry = "blackbetty@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.324215329356704"
lon = "12.375750271251132"
+++
gute, unverwüstliche Ware. Leider wurde der Anhänger vor ein paar Monaten in Großzschocher geklaut. Wer den Anhänger sieht, melde sich gerne bei uns!

