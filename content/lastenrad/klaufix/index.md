+++
title = "Klaufix"
description = "Anhänger"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Anhänger"},
  {name = "Standort", entry = "Breitkopfstraße, Reudnitz"},
  {name = "Beladung", entry = "90 kg"},
  {name = "Email", entry = "klaufix@kolara.org"},
  {name = "Details", entry = "Anhängerkupplung: 25mm Kugelkupplung (Standard an Sattelstange)"},
]
lat = "51.3351311019342"
lon = "12.398150430375905"
+++
grün, also umweltfreundlich

