+++
title = "Urszula"
description = "Long-John"
template = "zeug.html"

[extra]
thumbnail = "image.jpg"
properties = [
  {name = "Bautyp", entry = "Long-John"},
  {name = "Standort", entry = "Frau-Holle Weg, Marienbrunn"},
  {name = "Beladung", entry = "bis 80 kg"},
  {name = "Email", entry = "urszula@kolara.org"},
  {name = "Details", entry = "Scheibenbremsen, Nabenschaltung, Licht"},
]
lat = "51.3033463562914"
lon = "12.400727387963407"
+++
{{ gallery() }}
