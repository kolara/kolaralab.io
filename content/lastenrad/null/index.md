+++
title = "Null-Verbrauchi"
description = "Dreirad"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Elisabethstraße, Volkmarsdorf"},
  {name = "Beladung", entry = "100 kg"},
  {name = "Email", entry = "null@kolara.org"},
  {name = "Details", entry = "kleine Sitzmöglichkeit, optionales Regendach"},
]
lat = "51.34278696253463"
lon = "12.408826079730439"
+++


