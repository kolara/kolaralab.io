+++
date = 2017-02-18
title = "Noch mehr Räder"
+++

Wieder einmal haben es zwei Räder ins Kollektiv geschafft:

Hirschkolbensumach, ein Long-John-Boot mit großer Ladefläche, das im Radsfatz geboren wurde.

<img src="2017-02-18-1.jpg" alt="Foto" title="2017-02-18-1.jpg" />





Und -Bäcker to the future-, das als pflegebedürftiger Brötchenlieferant aus weiter Ferne zu uns fand und nun komplett überholt wurde. Als erstes Rad des Kollektives erschließt es Gohlis.

<img src="2017-02-18-2.jpg" alt="Foto" title="2017-02-18-2.jpg" />


