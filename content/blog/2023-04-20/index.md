+++
date = 2023-04-20
title = "RING FREI! am So. 09.07.2023"

[extra]
image = "IMG_6526.jpeg"
+++

Spazieren und spielen, wo sonst Autos fahren!
Zum Start in die Sommerferien feiern wir auf einem Stück vom Leipziger Ring mit einem breiten Bündnis den autofreien Sonntag. 

Erzählt es euren Freunden und Bekannten und stellt euch schon mal auf ein großes Spektakel ein! 

Im Rahmen des autofreien Sonntags lädt das Kollektiv Lastenrad zudem erneut zur Leipziger Lastenrad-Challange ein. In diesem Jahr wird mit einer partizipativen Performance Mobilitätskultur auf die Straße gebracht - oder um genauer zu sein: Auf den Ring! 

**Ort**: Tröndlinring, an der Blechbüchse / Richard-Wagner-Platz  
**Datum**: Sonntag, 09.07.2023

### Ablauf  
* **11:00** Eröffnung durch unsere Moderatorin von der [Theater Turbine](https://theaterturbine.de/)  
mit einer Eröffnungsansprache von Rosalie Kreuijer, [ADFC Leipzig](https://leipzig.adfc.de/)  
offene Lounge, Mitbring-Brunch, Spiele und vieles mehr  
* **11:30** Musik von den Plattenspielern von [audiobias](https://soundcloud.com/audiobias)  
* **13:00** Livemusik  
Diskussionen  
Einfahren der Strecke für die Leipziger Lastenradchallenge 2023  
* **14:00** Lastenrad-Theaterstück ["Leo und die geheimnissvollen Warenströme"](https://kolara.org/blog/2023-06-05/)  
* **15:30** Lastenradchallenge mit fulminante Sportmoderation von [Pillo](https://www.instagram.com/pillo_draws/),  
Eindrücke der Challenge aus dem letzten Jahr findet ihr [hier](https://kolara.org/blog/2022-08-05/) und [hier](https://kolara.org/blog/2021-09-21/), als wir schon mal auf einem Stück vom Ring gefahren sind.  
* **17:00** Siegerehrung der Lastenrad-Challenge  
* **17:15** Ausklang mit Musik von den Plattenspielern von [Carool](mixcloud.com/carool/)  

Veranstaltet vom Leipziger Bündnis "Ring frei",  
bestehend aus dem [Kollektiv Lastenrad](https://kolara.org/), [Greenpeace Leipzig](https://greenwire.greenpeace.de/greenpeace-leipzig/about), [BUND Leipzig](https://www.bund-leipzig.de/), [Fridays for Future](https://fridaysforfuture.de/ortsgruppen/leipzig/), [Verkehrswende Leipzig](https://verkehrswende-le.de/), [ADFC Leipzig](https://leipzig.adfc.de/), [Eine Welt Leipzig](https://einewelt-leipzig.de/de/) und [SUPERBLOCKS Leipzig](https://linktr.ee/superblocks.leipzig).  

Link zur Facebook-Veranstaltung: [Facebook-Link](https://www.facebook.com/events/108557112287604) (wenn das noch jemand nutzt;)  

{{ gallery() }} 