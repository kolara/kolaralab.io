+++
date = 2022-08-05
title = "Bericht der Leipziger Lastenrad-Challenge 2022"

[extra]
image = "R6__1099.jpg"
+++

Am Samstag, 16.07.22, nutzte das Kollektiv Lastenrad den Ferienstart und lud abermals zur [Lastenrad-Challenge](https://kolara.org/blog/2022-06-10/) ein. Neue und bekannte Gesichter lieferten sich Kopf-an-Kopf-Rennen und waren bunt gemischt: Selbstgeschweißte Räder fuhren gegen aktuelle Serienmodelle, Familienkutschen gegen Spezialräder.

Im Finale des Einzelfahrens konnte sich der wahnsinnige Eric P. mit selbstgebautem Lastenrad diesmal nicht durchsetzten – seine Gegner bereits überrundet setzte er offensichtlich selbst irritiert zur Ehrenrunde an und war so zeitlich chancenlos. Am Ende Staffelte Stand Christian oben auf dem Podium, gefolgt von Jost und Matilda. Wohl bestückt wurde der erste Platzt dieses Jahr mit einem Kaffee-Vollautomaten.

Im Team-Rennen konnte das Team der Gastgeber:innen „Kolhahahara“ den Titel verteidigen und die Team „eifrigen Eichhörnchen“ und „Ferdsch“ auf die folgenden Plätze verweisen.

Im Anschluss an das Rennen lockte die offene Strecke große und kleine Fahrer:innen zum rollenden Tagesausklang.

Auch der [MDR](https://www.mdr.de/nachrichten/sachsen/leipzig/leipzig-leipzig-land/lastenrad-challenge-wettbewerb-100.html) hat einen Bericht verfasst.

Unterstützt wurden Organisation, Abstimmung und Genehmigungsplanung von [Studio JoHey!](https://studio-johey.de) und [Juliana Klengel](https://radfahrliebe.de/). Gefördert wurde die Lastenrad-Challenge dieses Jahr aus dem Budget des den [Stadtbezirksbeirat Leipzig Süd](https://www.leipzig.de/buergerservice-und-verwaltung/stadtrat/stadtbezirksbeiraete/stadtbezirksbeirat-sued).

Danke an alle die da waren. Wir freuen uns schon auf das nächste Mal!

{{ gallery() }}
