+++
date = 2016-03-30
title = "Lastenradworkshop im Radsfatz"
+++

Seit langem gibt es mal wieder ein Lastenworkshop.

Wer möchte, kann sich am 1. und 2. April und am 8.und 9. April am Bau von ein oder zwei Lastenrädern im Radsfatz beteiligen.

Freitag von 14:00 – 20:00 Uhr

Samstag von 10:00 – 19:00 Uhr

Für diesen Workshop wollen wir keinen Stahl kaufen, sondern das Lastenrad komplett aus „Schrott“ aufbauen. Mit einem alten Tischgestell hatten wir da sehr gute Erfahrungen gesammelt. Um Verbrauchsmaterialien (Schutzgas, Elektroden, Flexscheiben, Schweißgerät, etc.) und Verpflegung zu bezahlen, wäre es nett, wenn ihr 20€ als Unkostenbeitrag aufbringen könntet.



<img src="2016-03-30.jpg" alt="tischgestell" title="2016-03-30.jpg" />

_Das Tischgestell_
