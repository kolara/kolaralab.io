+++
date = 2022-06-10
title = "Lastenrad-Challenge 2022! Am 16.07. auf der Rollschuhbahn in Lößnig."

[extra]
image = "LaRaCha_2022_Flyer.jpg"
hidetitle = true
+++
Lastenräder sind super!\
Egal ob neugierige Kinder, schwere Getränkekästen oder hipper Retro-Sessel: Mit Lastenrädern lässt sich eine Menge bewegen - ohne CO2 und [Stickoxide](https://www.umweltbundesamt.de/no2-krankheitslasten), ohne Parkplatzsuche und mit beiläufigem Workout.
Um das Bekannte unter Beweis zu stellen, laden wir zum Beginn der Sommerferien abermals zur Lastenrad-Challenge!
Bei zwei unterschiedlichen Formaten könnt ihr mit eigenen Rädern kommen oder eines der selbstgebauten [Lastenräder](./../../lastenrad/) von KoLaRa nutzen.\
Sowohl im Gruppen- und Einzelrennen als auch im Hindernis-Parcours geht es nicht nur um Schnelligkeit, sondern auch um Geschicklichkeit beim Beladen. Im Parcours können sich neben „Lastenrad-Profis“ auch Unerfahrene ausprobieren und ihr neues [Lieblingsgefährt](./../../lastenrad/) kennenlernen. Jede:r kann mitmachen!\
Begleitet wird die Veranstaltung von einem Sportmoderator und Musik und bietet damit auch für Zuschauer:innen einen kurzweiligen Nachmittag.
Also kommt rum! Weitersagen und Freund:innen mitbringen erlaubt 🙂
- Jede:r kann mitmachen !
- Verschiedene Disziplinen !
- Irre Siegerpreise !
- Helmpflicht für Teilnehmer:innen !

Datum: Samstag, 16.07.2022\
Ort: Rollschuhbahn im Erholungspark Lößnig-Dölitz (Koordinaten: 51.29424689348525, 12.40564080589061)

<!-- blank line -->
Eindrücke vom [letzten Mal findet ihr hier](./../2021-09-21/).\
Unterstützt wird die Veranstaltung dieses mal aus dem Budget des Stadtbezirksbeirats Süd.

<!-- blank line -->
Ablauf (Änderungen vorbehalten):

- ab 14 Uhr Anmeldung u. Einfahren der Strecke
- 14.30–15.30 Uhr Lastenrad-Rennen - Einzelfahren 
- 15.30–16.30 Uhr Lastenrad-Rennen - Teamfahren 
- 16.30–17.30 Uhr Geschicklichkeitsparcours
- 17.30 Uhr Sieger:innenehrung

<!-- blank line -->
Fragen? Dann schreibt uns gerne an kontakt[at]kolara[Punkt]org
