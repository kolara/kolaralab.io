+++
date = 2023-06-05
title = "Ein Theaterstück mit Lastenrädern am 09.07."

[extra]
image = "LaRaCha_LEO_2023_Web.jpg"
+++

Leo bestellt gerne im Internet: Kleidung, Geschirrspüler, Hamsterfutter, Lebenspartner:innen. Weil nicht alles in echt gefällt, was online schick aussieht, schickt sie regelmäßig wieder Dinge zurück. Was einfach klingt, sorgt auf Leipzigs Straßen für immer wildere Lieferverkehrsansammlungen.

In der von Erzähler:innen begleiteten Performance versuchen Lastenradfahrer:innen es Leo recht zu machen. Sie düsen um die Wette, laden auf ihre teils selbstgebauten Gefährte alles, was bestellt wird und müssen im Dauerbetrieb den nächsten Fahrer:innen Lastenrad und Ladung übergeben.  
Besucher:innen wird der ausufernde Wahnsinn des sorglosen Online-Shoppings als wildes Spektakel auf Leipzigs zentraler Straße geboten. Dabei steht es allen Besucher:innen offen, sich als Lieferant:innen aktiv zu beteiligen. 

Also kommt gerne rum! Wir freuen uns auf einen unterhaltsamen Nachmittag und sind gespannt, wie es am Ende ausgeht ;). Und wenn wenn ihr nicht nur zuschauen, sondern radelnd dabei sein wollt, dann schreibt an kontakt@kolara.org. Es wird ein Fest!

Die Veranstaltung findet im Rahmen von [Ring Frei!](https://kolara.org/blog/2023-04-20/) statt.  
Sie wird gefördert durch das [Kulturamt Leipzig](https://www.leipzig.de/buergerservice-und-verwaltung/aemter-und-behoerdengaenge/behoerden-und-dienstleistungen/dienststelle/kulturamt-41/)

__Datum__: Sonntag, 09.07.2023, 14:00 Uhr (Anmeldung zum Mitmachen und Einfahren der Strecke schon ab 13:00 Uhr)  
__Ort__: Tröndlinring, an der Blechbüchse  
Facebook-Veranstaltung: [Facebook-Link](https://www.facebook.com/events/546387170941535/) (wenn das noch jemand nutzt)  

_Veranstaltet vom Kollektiv Lastenrad Leipzig, unterstützt durch [Studio JoHey!](https://studio-johey.de) und [Klengel&Team](https://radfahrliebe.de/)._