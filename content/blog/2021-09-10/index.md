+++
date = 2021-09-10
title = "Ring frei! Am 19.09. ist der Leipziger Innenstadtring für Alle da."

[extra]
image = "Ring_frei.jpg"
hidetitle = "true"
+++
Lastenräder sind super!
Egal ob neugierige Kinder, schwere Getränkekästen oder hipper
Retro-Sessel: Mit Lastenrädern lässt sich eine Menge bewegen - ohne CO2
und [Stickoxide](https://www.umweltbundesamt.de/no2-krankheitslasten), ohne Parkplatzsuche und mit beiläufigem Workout.
Um das Bekannte unter Beweis zu stellen, laden wir nach der
erfolgreichen Lastenrad-Challenge Ende Juli jetzt zur Revanche!
Bei zwei unterschiedlichen Formaten könnt ihr mit eigenen Rädern kommen
oder eines der selbstgebauten [Lastenräder](https://kolara.org/lastenrad/) von #KoLaRa nutzen.
Im Hindernis-Parcours und Gruppenrennen geht es nicht nur um
Schnelligkeit, sondern auch um Geschicklichkeit beim Beladen. Neben
„Lastenrad-Profis“ können sich vor
allem im Parcours auch Unerfahrene ausprobieren und ihr neues
[Lieblingsgefährt](https://kolara.org/lastenrad/shirshasana/) kennenlernen. Jede:r kann mitmachen!
Begleitet wird die Veranstaltung von unserem Sportmoderator und bietet
damit auch für Zuschauer:innen einen kurzweiligen Nachmittag.
Also kommt rum! Weitersagen und Freund:innen mitbringen erlaubt 🙂
- Jede:r kann mitmachen !
- Verschiedene Disziplinen !
- Irre Siegerpreise !
- Helmpflicht für Teilnehmer:innen !
<!-- blank line -->
Eindrücke vom [letzten Mal](https://kolara.org/blog/2021-08-11/) findet ihr [hier](https://www.facebook.com/StudioJoHey/posts/986564265462109)
<!-- blank line -->
Ablauf:

- ab 13 Uhr Anmeldung u. Einfahren der Strecke
- 14–15.30 Uhr Lastenrad-Rennen (eher für Erfahrene)
- 15.45–17.30 Uhr Geschicklichkeitsparcours
- 17.30 Uhr Sieger:innenehrung

