+++
date = 2019-01-04
title = "Snake verschwunden"
[extra]
image = "2019-01-04.jpg"
+++
Wir als Kollektiv Lastenrad stellen Lastenräder zur freien Nutzung allen zur Verfügung. Wir wollen so unter anderem einen niedrigschwelligen Zugang zu Lastenrädern schaffen und diese als Alternative zum Auto im Alltag präsenter machen. Die meisten werden von uns selbst gebaut, es steckt in jedem Einzelstück eine Menge Energie, Zeit und Aufwand. Dennoch können sie einfach von allen genutzt werden, eine Meldung an die Radpat_innen genügt, ein Zersägen des Schlosses ist nicht notwendig.

Umso ärgerlicher ist es, dass Snake gerade vermisst wird.
**Es ist wieder da!**


Falls Irgendjemand etwas über den Verbleib weiß, sind wir über jeden Hinweis dankbar.

euer KoLaRa
