+++
date = 2013-09-09
title = "Lastenrad-Bauwochenende Ende Oktober"

[extra]
image = "img.jpg"
+++

Am **28./29. September** sowie am **5./6. Oktober** finden in den Nachbarschaftsgärten die nächsten **Schweiß-Workshops** statt. An beiden Wochenenden wird Evelyn jeweils eine theoretische Einführung in die Kunst des Schutzgas-Schweißens geben. Zudem kann das neue MIG-MAG-Gerät ausprobiert und/oder E-Hand verbessert werden. Geplant ist diesmal der Bau einer **Fahrrad-Abstellanlage, aber auch eigene, kleinere Reparaturarbeiten an mitgebrachten Gegenständen** sind willkommen. Die Teilnahme ist kostenfrei, fürs Mittagessen gibts nen Spendentopf.

An den beiden Wochenend-Workshops können jeweils **max. acht Leute teilnehmen**. Wenn Du mitmachen möchtest, **melde Dich bitte mit Angabe des Termins** unter: analui(at)posteo.de.
