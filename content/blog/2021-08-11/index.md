+++
date = 2021-08-11
title = "Großes Lastenradrennen beim Markt aus zweiter Hand"

[extra]
image = "LaRa_rennen.jpg"
+++

Nachhaltige Mobilität erfahrbar gemacht! 
Am Samstag, den 31.7. haben wir den Leipziger Augustplatz in eine Lastenrad-Rennstrecke [verwandelt](https://kolara.org/blog/2021-07-13/). Bei super Wetter ging es gute drei Stunden im Team-, Zeit- und Einzelrennen zur Sache! Dabei mussten die Pilot★innen nicht nur starke Beine mitbringen, sondern auch mit Geschick die widerspenstigen Ladungen bändigen. Mit 14 Einzelstarter★innen und weiteren vier Teams à drei Fahrer★innen wurde das Regendesaster der ersten Lastenrad-challenge vom September letzen Jahres weit übertroffen. Es gab dramtische Überholaktionen, starke Teamarbeit, Ringen um Sekunden in der Ladeboxengasse, umherfliegende Ladungsbestandteile und trotz eines Sturzes in der engen Kurvenschikane erfreulicherweise keine Verletzungen... und das alles vor einem enthusiastischen Publikum! 
<details>
<summary markdown="span"> Der Spaß am Lastenradeln war natürlich die Hauptsache, dennoch sollen <b>hier</b> die sportlichen Leistungen gewürdigt werden: </summary>
<br>

Bei den Teams konnte sich die „Schwarze Mamba“ durchsetzen, dicht gefolgt von „Die Grünen Leipzig“ mit neuem und jetzt erfolgreich eingeweihtem Lastenrad. Das gastgebende Team KoLaRa landete vor dem Team der [Verkehrswende Leipzig](https://verkehrswende-le.de/) auf dem dritten Platz.

Mit selbstgebautem KoLaRa-[Lastenrad](https://kolara.org/lastenrad/rakete/) sicherte Eric P. sich im Einzelzeitfahren die Bestzeit, Silber ging an Doro F., Bronze an Thomas G.

Im großen Finale des Einzelfahrens (Massenstart) war Anne G. die Schnellste, Anja B. ergatterte sich Platz 2. Sie profitierten dabei von einem Ladungsmalheur bei Eric P., der sich in dieser Disziplin mit Platz 3 zufriedengeben musste.

Das zwischenzeitlich eingeschobene Gehzeug-Rennen, ein Sprint zu Fuß mit Holzkonstruktionen so groß wie PKWs, war nicht nur lustig anzuschauen, sondern zeigte auch sehr plastisch, wie viel öffentlichen Raum Outos beanspruchen. Danke an unseren Sportmoderator Pillo P., dem auch nach vier Stunden der Mund nicht trocken wurde. Und danke an alle, die dabei waren, mitgefahren sind und geholfen haben. So schnell werden wir diesen Nachmittag nicht vergessen!
</details>

Stattgefunden hat die Veranstaltung im Rahmen des vom [BUND Leipzig](https://www.bund-leipzig.de/) organisierten „Markt aus zweiter Hand“. Die Ladegüter wurden uns von den anwesenden Initiativen zur Verfügung gestellt, u.a. Werkzeug vom [Café Kaputt](https://www.cafekaputt.de/), der von allen dankbar genutzten [Verschenke Kiste](https://verschenkekiste.de/), Wollknäuele von [kunzstoffe](https://kunzstoffe.de/) (rollen super weg) und Daunenjacken-Jacken vom Flohmarktladen [Liebenswertleipzig](https://liebenswert.hofgemeinde-leipzig.de/). Die Absperrung wurde freundlicherweise von der [Verkehrswacht Leipzig](https://www.verkehrswacht-leipzig.de/) zur Verfügung gestellt, die auch beim Aufbau geholfen hat. Und danke an rad3 für die Preise zum Einzelrennen Massenstart – mit dabei auch das ein oder andere von [CargoBike Jetzt!](https://www.cargobike.jetzt/)

Teaser: schon dieses Jahr, am Sonntag, den 19.9. wird es im Rahmen des Autofreien Sonntags eine Folgeveranstaltung geben. Wir freuen uns drauf.

{{ gallery() }}
