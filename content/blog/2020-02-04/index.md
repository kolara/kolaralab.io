+++
date = 2020-02-04
title = "Schweißworkshop 2020"
template = "page.html"
+++
Bald ist es wieder soweit: Auch 2020 soll es wieder einen Schweißworkshop geben.

<img src="2020-02-04-1.jpg" alt="flyer" title="2020-02-04-1.jpg" />

Ende März haben wir ein Wochenende rausgesucht an dem wir das Radsfatz belegen und dort Dinge schweißen werden:

    Fr. 20.03. 17.00 bis ca. 21.00 Uhr
    Sa. 21.03. ab 12.00 Uhr

Im letzten Jahr haben wir uns einen etwas konkreteren Plan gemacht und damit an dem Wochenende noch den Einkaufswagen gebaut. So ähnlich werden wir das wohl auch dieses Jahr handhaben, dazu später vielleicht noch mehr.

Ähnlich wie die letzten Jahre wäre ein kleiner Spendenbeitrag sinnvoll. Außerdem wäre es schön wenn wir kleine organisatorische Aufgaben (Essen & Material besorgen u.ä.) etwas verteilen könnten. Wer mitmachen möchte, melde sich kurz bei

<img src="2020-02-04-2.png" alt="flyer" title="2020-02-04-2.png" />
