+++
date = 2021-07-13
title = "Markt aus zweiter Hand mit großem Lastenradrennen"

[extra]
image = "flyer21LR.jpg"
hidetitle = "true"
+++

Es ist wieder soweit: Das große Lastenradrennen findet dieses Jahr im Rahmen des Marktes aus zweiter Hand am 31. Juli 2021 auf dem Augustusplatz in Leipzig statt. Bei unterschiedlichen Rennformaten könnt ihr mit eigenen Rädern kommen, oder eines der selbstgebauten Lastenräder von [KoLaRa](https://kolara.org/lastenrad/) nutzen. In Einzelzeitfahren, Gruppenrennen oder im Team geht es nicht nur um Schnelligkeit, sondern auch Geschicklichkeit beim Beladen. Neben "Lastenrad-Profis" können sich auch Unerfahrene ausprobieren und ihr neues [Lieblingsgefährt](https://kolara.org/lastenrad/shirshasana/) kennenlernen. Es gibt verrückte Test-Lasten, verrücktere Lastenräder, einen Sport-Moderator und wird sicher ein kurzweiliger Nachmittag. Also kommt rum! 

31.07.2021, 12 Uhr

Augustusplatz
