+++
date = 2023-07-23
title = "RING FREI! - KoLaRa berichtet!"

[extra]
image = "61-DSC_9144_copyright-ThomasPuschmann.jpg"
+++

„Ring frei!“ hieß es am Sonntag, dem 09.07.23, auf dem Leipziger Tröndlinring. Für einen Tag verwandelte ein breites Bündnis aus Initiativen und Vereinen die 3 bis 6 Autospuren in eine Fläche zum Schlendern, Spielen und Schwätzen. 

Ein gefülltes Programm schuf eine Vielzahl an Anlässen, den Leipziger Ring auf neue Weise zu erleben, während die freie Straße in sich selbst schon ein Erlebnis war.

So gestaltete der [BUND Leipzig](https://www.bund-leipzig.de/) ein Stück Asphalt in eine Minigolf-Bahn um. [Lila Lenker](http://lilalenker.de/) luden quer über fünf Spuren dazu ein, die Straße als Fahrrad-Parcours mit Hindernissen zu erleben. Beim Kleidertausch von [Greenpeace Leipzig](https://greenwire.greenpeace.de/greenpeace-leipzig/about) wurden noch gute und durchaus stylische Klamotten getauscht. Die [Verkehrswende Leipzig](https://verkehrswende-le.de) nutzte die gewonnen Freifläche für einen Wanderbaum und ein Falschparker-Quiz. Der [VCD Elbe-Saale](https://elbe-saale.vcd.org/) veranstaltete direkt auf der Straße eine Umfrage, wie die vielen Fahrspuren auch anderweitig genutzt werden könnten.

Von KoLaRa, dem [Kollektiv Lastenrad](https://kolara.org/), gab es dieses Jahr neben der obligatorischen Lastenrad-Challenge ein eigens entwickeltes Theaterstück mit Lastenrädern. Und für alle, die weniger auf Action und mehr auf Austausch aus waren, boten der [ADFC Leipzig](https://leipzig.adfc.de/), der [Eine Welt e.V. Leipzig](https://einewelt-leipzig.de/de/) und das [Klimabüro der Verbraucherzentrale Sachsen](https://www.verbraucherzentrale-sachsen.de/klimabuero) mit Informationsangeboten gute Gelegenheiten um ins Gespräch zu kommen.

Locker improvisiert moderierten Raschid und Sarah von der [Theater Turbine](https://theaterturbine.de/) das komplette Programm.

Zum Auftakt brachte Rosali vom ADFC Leipzig die Missstände der aktuellen Verkehrssituation in Leipzig auf den Punkt, zeigte aber auch Möglichkeiten für ein besseres und nachhaltigeres Miteinander im Straßenverkehr auf. Anschließend teilte Martin Reinhardt von Greenpeace Leipzig, selbst leidenschaftlicher Radfahrer, Fußgänger und Bahnfahrer, seine Gedanken zur Leipziger Mobilität und rief zur Abkehr vom motorisierten Individualverkehr auf. 

Im Anschluss zeigten komma6 mit ihrem Set, dass sich der Ring gut als Tanzfläche eignet. Im folgenden Interview mit Raschid und Sarah erzählte Ulrike Gebhardt, Mitglied im Stadtbezirksbeirat Ost, was Sie antreibt, sich für die Mobilitätswende in Leipzig einzusetzen und welche Erfolge sich bereits einstellen.

Daran anknüpfend spielten Green Moments mit Piano und Bass groovige Songs, bevor das Lastenradtheaterstück seine Premiere feierte. 

In dem partizipativen Stück ["Leo und die geheimnisvollen Warenströme"](https://kolara.org/blog/2023-06-05/) führte der Leipziger Sprecher [Hans Nenoff](https://www.hans-nenoff.de/) die Zuschauerinnen und Zuschauer durch die Geschichte. Bei Zwischenszenen wechselte er sich mit unserem „Sportmoderator“ [Pillo](https://www.instagram.com/pillo_draws/) ab.
Neben den zwei Laienschauspielerinnen, die die / den Protagonist:in Leo und eine Schlange spielten, beteiligten sich weitere Personen aus dem Publikum als „Lieferdienste“ mit ihren Lastenrädern. 

Das künstlerisch überzeichnete Stück begann mit dem Sündenfall der Hauptfigur an den Konsum, gefolgt von sich aufschaukelnden Lieferwettkämpfen und endet mit heroischer Selbstbefreiung aus Letztgenanntem. Unterstützt wurde KoLaRa bei der Umsetzung durch Leihgaben des [Leipziger Ostpassagetheater](https://ost-passage-theater.de/). Finanziell unterstützt durch das Kulturamt der Stadt Leipzig und den Stadtbezirksbeirat Mitte. 

Während der Bühnenumbau hin zum Lastenrad-Challange-Parcour erfolgte, beglückte DJ [audiobias](https://soundcloud.com/audiobias) die Anwesenden mit entspannten Sounds von seinen Turntables.

Bei der diesjährigen Lastenrad-Challenge hielten sich die Teilnehmerinnen und Teilnehmer trotz stolzer Temperaturen des bisher heißesten Tag des Jahres nicht mit ihrer Leistung zurück. Vielleicht auch angespornt durch die in diesem Jahr zu gewinnenden Freikarten für das Ancient Trance Festival wurde sich bis ins Finale ein Kopf-an-Kopf-Rennen von ein- und mehrspurigen Lastenrädern geliefert.

Zum Ausklang war [Carool](https://www.mixcloud.com/carool/) an den Plattentellern, bevor die Straße wieder für den Autoverkehr freigegeben wurde und der Wunsch nach einer Wiederholung blieb.

Die ganze Veranstaltung lief trotz der hohen Temperaturen reibungslos ab, auch weil sie frühzeitig mit der Stadt abgestimmt war und von vielen tatkräftig unterstützt wurde. Ein Awareness-Team von [Fridays for Future](https://fridaysforfuture.de/ortsgruppen/leipzig/) so wie Ersthelfer waren während der ganzen Zeit anwesend und stellten das Wohlbefinden der Besuchenden sicher. Natürlich hatten wir uns mehr Besucherinnen und Besucher gewünscht, die mit uns das Stücke autofreien Sonntag feiern. Wir verstehen aber auch, dass der bisher heißeste Tag angenehmere Flächen geboten hat, als eine unverschattete, asphaltierte Straße. 

Organisatorisch und konzeptionell wurde die Veranstaltung durch [Studio JoHey!](https://studio-johey.de/) und [Juliana Klengel](https://radfahrliebe.de/) unterstützt. Es berichteten u.a. der MDR im Bewegtbild und die LVZ in einem eigenen Beitrag. 

Wir danken allen Initiativen, Vereinen, Institutionen und Einzelpersonen, die diesen Tag möglich gemacht haben und damit auch erlebbar werden ließen, was auf dem Leipziger Ring möglich ist und hoffen, dass weitere autofreie Tage in Leipzig folgen.

Veranstaltet wurde das Stück autofreier Sonntag vom Leipziger Bündnis "Ring frei",  
bestehend aus dem [Kollektiv Lastenrad](https://kolara.org/), [Greenpeace Leipzig](https://greenwire.greenpeace.de/greenpeace-leipzig/about), [BUND Leipzig](https://www.bund-leipzig.de/), [Fridays for Future](https://fridaysforfuture.de/ortsgruppen/leipzig/), [Verkehrswende Leipzig](https://verkehrswende-le.de/), [ADFC Leipzig](https://leipzig.adfc.de/), [Eine Welt Leipzig](https://einewelt-leipzig.de/de/) und [SUPERBLOCKS Leipzig](https://linktr.ee/superblocks.leipzig).  

{{ gallery() }}
