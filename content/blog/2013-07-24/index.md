+++
date = 2013-07-24

title = "Siebfreak und Co. fast fertig – Schutzgas-Schweißen im September"

[extra]
image = "img.jpg"
+++

Letztes Wochenende ist der „Siebfreak“ beachtlich gewachsen und wartet jetzt nur noch auf kleinere Metamorphosen, um anschließend zu 100 % StVO-konform über Leipzigs Straßen zu rollen. Zudem gab es erfreulichen Nachwuchs: das Dreirad „Eblie“ hat den Weg in die Nachbarschaftsgärten gefunden und wurde ebenso wie der Long-John „Mordock“ und das „Skateboard-Lasti“ gepimpt.

Wir gönnen uns jetzt erstmal eine kleine Sommerpause und treffen uns in Kleingruppen, um zu bauen und uns den Verleih sowie die nächsten Aktionen zu überlegen. Wenn Du Lust/Zeit hast bei irgendetwas mitzuwirken, schreibe einfach eine Mail!

Im September wird es dann einen MIG-MAG-Schweiß-Workshop mit einer diplomierten Schweißerin geben, bei dem die Grundlagen des Schutzgas-Schweißens vermittelt und wieder selbst gebrutzelt werden kann.

Bis dahin, Sommer ahoi!