+++
date = 2021-09-21
title = "Lastenrad-Challenge auf dem Ring"

[extra]
image = "DSCF8249_small.jpg"
+++

Am Sonntag, 19.09.21 nutzte das Kollektiv Lastenrad den autofreien Ring in Leipzig und lud abermals zur [Lastenrad-Challenge](./../2021-09-10/) ein. Neue und bekannte Gesichter lieferten sich ein Kopf-an-Kopf-Rennen. Nicht nur einmal wurden die Positionen im Feld dabei durch die Ladevorgänge neu sortiert. Am Ende waren die Zweispurer (dreirädrige Lastenräder) auf dem langen, geraden Straßenabschnitt trotz großer Ladeflächen den zweirädrigen Einspurern chancenlos unterlegen.
 
Teilnehmende Fahrer:innen und Räder waren bunt gemischt: Selbstgeschweißte Räder fuhren gegen aktuelle Serienmodelle, holländische Oldtimer gegen Familienkutschen und mobile Infostände. So war unter anderem auch das [Verkehrs- und Tiefbauamt] (https://www.leipzig.de/buergerservice-und-verwaltung/aemter-und-behoerdengaenge/behoerden-und-dienstleistungen/dienststelle/verkehrs--und-tiefbauamt-66) der Stadt Leipzig mit einem Lastenrad vertreten.

Im Finale des Einzelfahrens konnte sich der wahnsinnige Eric P. mit selbstgebautem Lastenrad den ersten Platz sichern – nicht zuletzt auf Grund seiner ausgefeilten Ladetechnik. Die Plätze zwei und drei gingen an die Team-Kollegen Peter E. und Anne G. aus dem Team der „intergalaktischen Pinguine“. Schnellster Mehrspurer wurde Jan „The Beast“.

Im Team-Rennen holten das "grüne Team" Silber und die intergalaktischen Pinguine Silber-Gold. Das gastgebende Team von KoLaRa konnte sich nach letztmalig drittem Platz dieses mal die goldene Pedale sichern.

Im Anschluss an das Rennen lockte ein Geschicklichkeits-Parcours die Fahrer:innen mit Feingefühl. Hier sicherte sich ein Überraschungsgast mit dreister, aber effektiver Technik den ersten Platz.

Das Rennen und der Parcours fand im Rahmen der [Gehzeug-Parade der Verkehrswende Leipzig] (https://verkehrswende-le.de/event/die-groesste-gehzeugparade-der-welt/) statt, bei der über 100 Gehzeuge zusammenkamen – ein Weltrekord. Atmosphärisch wurden das Rennen und die gesamte Veranstaltung von Teilen der [Theater-Turbine] (https://theaterturbine.de/) im Modus einer Sportberichterstattung moderiert.

Danke an alle die da waren. Wir freuen uns schon auf das nächste Mal! :)

{{ gallery() }}
