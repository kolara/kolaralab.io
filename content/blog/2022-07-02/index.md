+++
date = 2022-07-02
title = "Schweißworkshop 2022"
template = "page.html"
+++
Nachdem die letzten Jahre alles immer wieder ausgefallen ist, wollen wir uns dieses Jahr lieber sehr spontan als garnicht treffen. Deshalb ist es schon nächstes Wochenende soweit: am 09.07.2022 soll geschweißt werden!

Derzeit planen wir erstmal nur den Samstag das [Radsfatz](https://radsfatz.org) zu belegen und dort Dinge, wie einen Zaun und ein Lastenrad zu schweißen und mal sehen was noch. Die Uhrzeit ist flexibel, aber es wird wohl am späten Vormittag losgehen (vielleicht so ab 11 uhr). Sollte Bedarf bestehen, erweitern wir das ganze vielleicht auch auf Freitag oder Sonntag.

Ähnlich wie die letzten Male wäre ein kleiner Spendenbeitrag sinnvoll, damit wir weiter die Werkstatt und auch unser Lastenrad Kollektiv weiter [existieren](https://eisi113b.blackblogs.org) lassen können.  Wer mitmachen möchte, melde sich gerne bei [shirshasana](https://kolara.org/lastenrad/shirshasana/), ebenso wer Fragen hat. Bis nächste Woche!
