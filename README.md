# kolara.gitlab.io

Die Website benutzt den static site generator [Zola](https://getzola.org). Dieses Repo benutzt CI und Gitlab-Pages. Das heißt, das die Website automatisch nach jeder änderung von Gitlab neu gebaut wird. Danach liegt sie als statische Seite vor.

## Benutzung
Die gesamte Seite liegt in diesem Git-Repository. Es sollte vermieden werden Inhalte aus externen Quellen einzubinden.


### inhaltliche Änderungen
Der gesamte Inhalt befindet sich in im Ordner `/content`.
Alle inhaltlichen Änderungen sollten hier stattfinden. Die Dateien sind in [Markdown](https://daringfireball.net/projects/markdown/syntax) geschrieben und besitzen eine Kopfzeile, in der Dinge wie *Title*, *Datum* oder andere Dinge festgelegt werden können. 
Ziel ist es die Dateien selbsterklärend und Menschenlesbar zu halten.

#### Bilder einbinden
Anstatt Bilder mit dem normalen Markdown-Syntax einzubinden, lässt sich auch
```
  {{ image(name="IMG_1680.jpeg") }}
```
verwenden. Dies ist ein Shortcode, der unter `templates/shortcodes` definiert ist.
Dies hat den Vorteil, das die Bilder vorgerendert werden und so in der Regel schneller laden.

### gestalterische Änderungen
Das Aussehen der Seite ergibt sich aus den Templates (in `/templates`) und den von den Templatesgenutzten Dateien. Diese sollten keinen Inhalt enthalten, sondern den Inhalt aus `/content` einbinden. Die templates nutzen die Sprach [Tera](https://tera.netlify.app/). Bessere Dokumentation findet sich aber möglicherweise bei [jinja2](https://jinja.palletsprojects.com/en/2.11.x/), die Tera sehr ähnlich ist.
Die Stylesheets nutzen [sass bzw. scss](https://sass-lang.com/guide).


## TODO

* Alte artikel übernehmen
* css für gallery shortcode schreiben
* auf der karte: unterschiedliche farben für unterschiedliche fahrzeuge
* Add [open graph metadata](https://ogp.me/)
* Check layouts for smartphone compatibility (hero page should change font size)
* nav icon erstellen
* Kolara logo webtauglich machen
